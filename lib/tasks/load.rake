require 'csv'

namespace :load do
  desc 'loads data to books table'
  task data: :environment do
    ARGV.each { |a| task a.to_sym do; end }
    filename = ARGV[1]

    CSV.foreach(filename, headers: true) do |row|
      Book.create!(row.to_hash)
    end
  end
end
