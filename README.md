# README

Wellcome, this readme is intended to help you to setup the project locally in no time.

* Before start
have docker and docker-compose installed locally.

* Getting started
The project has a Dockerfile so if you already has docker installed, follow this steps:
  - `docker-compose build`
  - `docker-compose exec web rails db:create db:migrate` 
  - `docker-compose exec web rails load:data sample.csv` 
  - `docker-compose up` 

the sample.csv file has some sample data and the `load:data` rake task loads a csv file into your local database.

now you can use the api to search api books by title or author:
```
curl 'localhost:3000/api/v1/search?q=fundamentals'
["Fundamentals of Wavelets Goswami, Jaideva"]%
curl 'localhost:3000/api/v1/search?q=Frank'
["Image Processing \u0026 Mathematical Morphology Shih, Frank","Trial, The Kafka, Frank"]%
```

* Running tests
 - `docker-compose exec web bundle exec rspec`
