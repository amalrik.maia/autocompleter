#frozen_string_literal: true

module Api::V1
  class SearchController < ApplicationController
    def index
      @results = Book.search(params[:q])
      render json: @results
    end
  end
end
