class Book < ApplicationRecord
  include PgSearch::Model
  multisearchable against: [:title, :author]

  validates :title, :author, :genre, :height, :publisher, presence: true

  def self.search(term)
    PgSearch.multisearch(term).pluck(:content)
  end
end
