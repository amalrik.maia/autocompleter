# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Book, type: :model do
  let(:books) { create_list(:book, 10) }

  describe 'validations' do
    it { should validate_presence_of :title }
    it { should validate_presence_of :author }
    it { should validate_presence_of :genre }
    it { should validate_presence_of :height }
    it { should validate_presence_of :publisher }
  end

  it 'has a valid factory' do
    expect(build(:book)).to be_valid
  end

  describe '.search' do
    context 'when a book with the term exists' do
      it 'returns the searched term' do
        expect(Book.search(books.first.title)).to include("#{Book.first.title} #{Book.first.author}")
      end
    end

    context 'when a book with the term dont exists' do
      it 'returns an empty array' do
        expect(Book.search('')).to eq([])
      end
    end
  end
end
