FactoryBot.define do
  factory :book do
    title { Faker::Book.title }
    author { Faker::Book.author }
    genre { Faker::Book.genre }
    height { rand(999) }
    publisher { Faker::Book.publisher }
  end
end
