require 'rails_helper'

RSpec.describe 'API::V1::Search', type: :request do
  let(:books) { create_list(:book, 10) }

  context 'when the a book with the searched term exists' do
    it 'returns a success http status' do
      get api_v1_search_path(q: books.first.title)
      expect(response.status).to eq 200
    end

    it 'returns an array with matching terms' do
      get api_v1_search_path(q: books.first.title)
      expect(response.body).to include("#{Book.first.title} #{Book.first.author}")
      expect(JSON.parse(response.body)).to be_an(Array)
    end
  end
end
