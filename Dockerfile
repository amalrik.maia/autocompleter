FROM ruby:2.6
RUN apt-get update -qq && apt-get install -y nodejs postgresql-client

RUN mkdir /project
COPY Gemfile Gemfile.lock /project/
WORKDIR /project
RUN gem install bundler
RUN bundle install
COPY . /project

EXPOSE 3000

# Start the main process.
CMD ["rails", "server", "-b", "0.0.0.0"]



